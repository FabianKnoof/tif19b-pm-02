package com.halp.halp.Usermanagement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Department")
public class Department {

    @Id
    private String id;

    @Indexed(unique = true, direction = IndexDirection.DESCENDING)

    private String department;

    private int chatAccessed = 0;

    public Department(String department){
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getChatAccessed() {
        return chatAccessed;
    }

    public void chatAccessed(){
        chatAccessed ++;
    }
}
