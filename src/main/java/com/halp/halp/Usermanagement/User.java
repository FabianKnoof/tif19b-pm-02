package com.halp.halp.Usermanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


@Document(collection = "User")
public class User {

    @Id
    private String id;
    @Indexed(unique = true, direction = IndexDirection.DESCENDING)
    private String email;
    private String password;
    private String forename;
    private String surname;
    private boolean enabled;

    @DBRef
    private Set<Role> roles = new HashSet<>();
    private boolean admin;
    private boolean maintainer;

    @DBRef
    private Department department;

    @Autowired
    private RoleRepository roleRepository;

    private int chatAccessed = 0;

    public User() {
    }

    public User(String email, String password, String forename, String surname) {
        this.email = email;
        this.password = password;
        this.forename = forename;
        this.surname = surname;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void removeRole(Role role) {
        this.roles.remove(role);
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isMaintainer() {
        return maintainer;
    }

    public void setMaintainer(boolean maintainer) {
        this.maintainer = maintainer;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getChatAccessed() {
        return chatAccessed;
    }

    public void chatAccessed(){
        chatAccessed ++;
    }
}
