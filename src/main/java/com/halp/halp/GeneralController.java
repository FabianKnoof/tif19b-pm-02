package com.halp.halp;

import com.halp.halp.QuestionAnswer.QuestionAnswer;
import com.halp.halp.QuestionAnswer.QuestionAnswersDatabase;
import com.halp.halp.Usermanagement.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GeneralController {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CustomUserDetailsService userDetailsService;
    @Autowired
    private QuestionAnswersDatabase questionAnswersDatabase;
    @Autowired
    private DepartmentRepository departmentRepository;

    @GetMapping({"/", "/home"})
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("home");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            User user = userDetailsService.findUserByEmail(((org.springframework.security.core.userdetails.User) principal).getUsername());

            modelAndView.addObject("user", user);
            modelAndView.addObject("logout", true);
        } else {
            modelAndView.addObject("login", true);
        }

        return modelAndView;
    }

    @GetMapping("/chat")
    public ModelAndView chat() {
        ModelAndView modelAndView = new ModelAndView("chat");

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userDetailsService.findUserByEmail(((org.springframework.security.core.userdetails.User) principal).getUsername());

        user.chatAccessed();
        userRepository.save(user);

        Department department = departmentRepository.findByDepartment(user.getDepartment().getDepartment());
        department.chatAccessed();
        departmentRepository.save(department);

        return modelAndView;
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/adminUI")
    public ModelAndView adminUI() {
        return getAdminModelAndView();
    }

    @PostMapping("/adminUI")
    public ModelAndView newUser(@ModelAttribute("newUser") User user) {
        ModelAndView modelAndView = getAdminModelAndView();

        if (userRepository.existsByEmail(user.getEmail())){
            modelAndView.addObject("userExists", "The user already exists!");
        } else {
            if (user.isAdmin()){
                user.addRole(roleRepository.findByRole("admin"));
            }
            if (user.isMaintainer()){
                user.addRole(roleRepository.findByRole("maintainer"));
            }

            Department department = departmentRepository.findByDepartment(user.getDepartment().getDepartment());
            user.setDepartment(department);

            userDetailsService.saveUser(user);
        }

        return modelAndView;
    }


    private ModelAndView getAdminModelAndView() {
        ModelAndView modelAndView = new ModelAndView("adminUI");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userDetailsService.findUserByEmail(((org.springframework.security.core.userdetails.User) principal).getUsername());
        modelAndView.addObject("currentUser", user);

        modelAndView.addObject("newUser", new User());

        modelAndView.addObject("userList", userRepository.findAll());
        modelAndView.addObject("departmentList", departmentRepository.findAll());

        List<Role> roleList = roleRepository.findAll().stream().filter(role -> !role.getRole().equals("user")).collect(Collectors.toList());
        modelAndView.addObject("roleList", roleList);

        return modelAndView;
    }

    @GetMapping("/maintainerUI")
    public ModelAndView maintainerUI() {
        return getMaintainerModelAndView();
    }

    @PostMapping("/maintainerUI")
    public ModelAndView newQuestionAnswer(@ModelAttribute("questionKeys") String questionKeys, @ModelAttribute("answer") String answer, @ModelAttribute("language") String language) {
        ModelAndView modelAndView = getMaintainerModelAndView();

        ArrayList<String> questionKeysList = new ArrayList<String>(Arrays.asList(questionKeys.split(",")));
        for (int i = questionKeysList.size() - 1; i >= 0; i--) {
            questionKeysList.set(i, questionKeysList.get(i).trim().toLowerCase());
            if (questionKeysList.get(i).isEmpty()) {
                questionKeysList.remove(i);
            }
        }
        QuestionAnswer questionAnswer = new QuestionAnswer(questionKeysList, answer, QuestionAnswer.Language.EN);
        if (language.equals("DE")) {
            questionAnswer.setLanguage(QuestionAnswer.Language.DE);
        }
        questionAnswersDatabase.save(questionAnswer);

        return modelAndView;
    }

    private ModelAndView getMaintainerModelAndView() {
        ModelAndView modelAndView = new ModelAndView("maintainerUI");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userDetailsService.findUserByEmail(((org.springframework.security.core.userdetails.User) principal).getUsername());
        modelAndView.addObject("user", user);

        modelAndView.addObject("questionAnswerList", questionAnswersDatabase.findAll());

        return modelAndView;
    }

    @GetMapping("/test")
    public String test() {
        return "home";
    }
}
