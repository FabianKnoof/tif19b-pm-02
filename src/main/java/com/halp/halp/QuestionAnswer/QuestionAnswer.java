package com.halp.halp.QuestionAnswer;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "QuestionAnswer")
public class QuestionAnswer {

    public enum Language {
        DE, EN
    }

    @Id
    public String id;

    public ArrayList<String> questionKeys;

    public String answer;

    public Language language;

    public QuestionAnswer(){}

    public QuestionAnswer(ArrayList<String> questionKeys, String answer, Language language) {
        this.questionKeys = new ArrayList<String>();
        addQuestionKeys(questionKeys);
        this.answer = answer;
        this.language = language;
    }

    public void addQuestionKeys(ArrayList<String> keys) {
        for (int i = 0; i < questionKeys.size(); i++) {
            questionKeys.set(i, questionKeys.get(i).toLowerCase());
        }
        questionKeys.addAll(keys);
    }

    public void addQuestionKey(String key) {
        questionKeys.add(key.toLowerCase());
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public ArrayList<String> getQuestionKeys() {
        return questionKeys;
    }

    public String getAnswer() {
        return answer;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
