package com.halp.halp.QuestionAnswer;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionAnswersDatabase extends MongoRepository<QuestionAnswer, String> {

}
