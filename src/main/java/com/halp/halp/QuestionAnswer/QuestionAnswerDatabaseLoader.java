package com.halp.halp.QuestionAnswer;

import com.halp.halp.Messaging.HALpBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class QuestionAnswerDatabaseLoader {

    @Autowired
    private QuestionAnswersDatabase questionAnswersDatabase;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    private final static String botName = HALpBot.getBotName();

    public void loadDummyQuestionsAnswer(String[] keys, String answer, QuestionAnswer.Language language) {
        ArrayList<String> questionKeys = new ArrayList<String>();
        questionKeys.addAll(Arrays.asList(keys));
        questionAnswersDatabase.save(new QuestionAnswer(questionKeys, answer, language));
    }

}
