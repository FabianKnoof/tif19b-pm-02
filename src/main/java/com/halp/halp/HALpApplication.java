package com.halp.halp;

import com.halp.halp.Usermanagement.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

@SpringBootApplication
public class HALpApplication {

    public static void main(String[] args) {
        SpringApplication.run(HALpApplication.class, args);
    }

    @Bean
    CommandLineRunner init(UserRepository userRepository, CustomUserDetailsService userDetailsService, RoleRepository roleRepository, DepartmentRepository departmentRepository) {

        return args -> {
// Do something before start
            addDepartment("CEO", departmentRepository);
            addDepartment("Strategy", departmentRepository);
            addDepartment("Planning", departmentRepository);
            addDepartment("Support", departmentRepository);
            addDepartment("Marketing", departmentRepository);
            addDepartment("Key-Account-Management", departmentRepository);
            addDepartment("Purchasing", departmentRepository);
            addDepartment("Controlling", departmentRepository);
            addDepartment("Human resources", departmentRepository);
            addDepartment("Project management", departmentRepository);
            addDepartment("PL-Pool", departmentRepository);
            addDepartment("Product management", departmentRepository);


            User admin = new User("admin@admin.de", "admin", "Admin", "Admin");
            if (!userRepository.existsByEmail(admin.getEmail())){
                admin.addRole(roleRepository.findByRole("admin"));
                admin.setDepartment(departmentRepository.findByDepartment("CEO"));
                userDetailsService.saveUser(admin);
            }

            User maintainer = new User("maintainer@maintainer.de", "maintainer", "Maintainer", "Maintainer");
            if (!userRepository.existsByEmail(maintainer.getEmail())){
                maintainer.addRole(roleRepository.findByRole("maintainer"));
                maintainer.setDepartment(departmentRepository.findByDepartment("CEO"));
                userDetailsService.saveUser(maintainer);
            }

            User test = new User("test@test.de", "test", "Test", "Test");
            if (!userRepository.existsByEmail(test.getEmail())){
                test.setDepartment(departmentRepository.findByDepartment("CEO"));
                userDetailsService.saveUser(test);
            }



        };
    }

    private void addDepartment(String departmentString, DepartmentRepository departmentRepository) {
        Department department = departmentRepository.findByDepartment(departmentString);
        if (department == null) {
            Department newDepartment = new Department(departmentString);
            departmentRepository.save(newDepartment);
        }
    }
}
