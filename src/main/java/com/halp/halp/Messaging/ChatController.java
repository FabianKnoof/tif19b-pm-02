package com.halp.halp.Messaging;

import com.halp.halp.QuestionAnswer.QuestionAnswer;
import com.halp.halp.QuestionAnswer.QuestionAnswerDatabaseLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {

    @Autowired
    private HALpBot bot;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private QuestionAnswerDatabaseLoader questionAnswerDatabaseLoader;

    @MessageMapping("/chat.sendMessage")
    public void sendMessage(@Payload ChatMessage chatMessage) {
        messagingTemplate.convertAndSend("/topic/" + chatMessage.getSender(), chatMessage);
        if (chatMessage.getType() == ChatMessage.MessageType.CHAT && chatMessage.getSender() != bot.getBotName()) {
            bot.getMessage(chatMessage);
        }
    }

    @MessageMapping("/chat.addUser")
    public void addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        if (chatMessage.getSender().contains("load data")) {
            questionAnswerDatabaseLoader.loadDummyQuestionsAnswer(new String[]{"hi", "hello", "welcome",}, "Hello there User!", QuestionAnswer.Language.EN);
        }
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        messagingTemplate.convertAndSend("/topic/" + chatMessage.getSender(), chatMessage);
    }
}
