package com.halp.halp.Messaging;

import com.halp.halp.QuestionAnswer.QuestionAnswer;
import com.halp.halp.QuestionAnswer.QuestionAnswersDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HALpBot {

    private final static String botName = "HALp9000";

    @Autowired
    private ChatController controller;

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private QuestionAnswersDatabase questionAnswersDatabase;

    private int wrongAnswerCounter = 0;

    public void getMessage(ChatMessage chatMessage) {


        QuestionAnswer questionAnswer = getAnswer(chatMessage);
        if (questionAnswer == null) {
            messagingTemplate.convertAndSend("/topic/" + chatMessage.getSender(), new ChatMessage(ChatMessage.MessageType.CHAT, "Sorry, I couldn't understand that. Try it a different way.", botName));
            wrongAnswer(chatMessage.getSender());
        } else {
            messagingTemplate.convertAndSend("/topic/" + chatMessage.getSender(), new ChatMessage(ChatMessage.MessageType.CHAT, questionAnswer.getAnswer(), botName));
        }
    }

    private QuestionAnswer getAnswer(ChatMessage chatMessage) {
        String message = chatMessage.getContent();
        for (QuestionAnswer questionAnswer : questionAnswersDatabase.findAll()) {
            for (String questionKey : questionAnswer.getQuestionKeys()) {
                String regex = "(\\b" + questionKey.toLowerCase().trim() + "\\b)";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(message.toLowerCase().trim());
                if (matcher.find()) {
                    return questionAnswer;
                }
            }
        }
        return null;
    }

    private void wrongAnswer(String sender) {
        wrongAnswerCounter++;
        if (wrongAnswerCounter >= 3) {
            messagingTemplate.convertAndSend("/topic/" + sender, new ChatMessage(ChatMessage.MessageType.CHAT, "I'm sorry but I can't help you. You may try to find the term you'd like to know about on this external site: https://www.smartsheet.com/complete-glossary-project-management-terminology", botName));
            wrongAnswerCounter = 0;
        }
    }

    public static String getBotName() {
        return botName;
    }
}
